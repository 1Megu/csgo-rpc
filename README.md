# CS:GO Discord Rich Presence
This is a small project I've been working on to show live match data using Discord's rich presence feature. My implementation is clean and relies only on features provided within the game. This means it is entirely VAC and ESEA/Faceit safe.

This was built in NodeJS but has a very small footprint so can be left running in the background without any performance impact.

## Features
_needs completing_

## Installing
1) Install the required config `cfg\gamestate_integration_discordrpc.cfg` to your CSGO install directory, usually `C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive\csgo\cfg`
2) Install the dependencies with `npm install`
3) Use `node .` to run the client

That's it. I have only tested this on Windows but it should work for MacOS and Linux installs too substituting the correct game location.

## Feedback
- Feel free to submit PR's with improvements
- Feel free to create issues if you encounter any bugs