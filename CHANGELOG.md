### 0.0.1 - 05/02/2020
- Initial release
- Supports Competitive, Casual, Deathmatch and Wingman
- Works with aim_redline, cs_office, de_cache, de_cbble, de_dust2, de_inferno, de_lake, de_mirage, de_nuke, de_overpass, de_train, de_vertigo, gd_cbble and surf_rookie