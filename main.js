// Discord RPC for CS:GO
// by Jimmy
// 2020-02-05

const express = require('express');
const DiscordRPC = require('discord-rpc');
const bodyParser = require('body-parser');
const { exec } = require('child_process');

const app = express();
const port = 3000;

const clientId = '674435251099009062';
DiscordRPC.register(clientId);

const rpc = new DiscordRPC.Client({ transport: 'ipc' });

const matchLive = {
  isLive: false,
  startTime: '',
};

function getTimestamp() {
  if (matchLive.isLive) return matchLive.startTime;
  matchLive.isLive = true;
  matchLive.startTime = Date.now();
  return matchLive.startTime;
}

function setActivity(json) {
  if (!rpc) {
    return;
  }

  rpc.setActivity({
    details: json.details,
    state: json.state,
    startTimestamp: json.timestamp,
    largeImageKey: json.largeImageKey,
    largeImageText: json.largeImageText,
    smallImageKey: json.smallImageKey,
    smallImageText: json.smallImageText,
    instance: false,
  });
}

function capitalFirst(string) {
  return string.replace(string.charAt(0), string.charAt(0).toUpperCase());
}

function parseActivity(request) {
  if (request.player.activity === 'menu') {
    matchLive.isLive = false;
    setActivity({
      details: 'In Menu',
      state: 'Idle',
      largeImageKey: 'icon',
      largeImageText: 'In Menu',
      smallImageKey: 'none',
      smallImageText: 'none',
      timestamp: '',
    });
  }
  if (
    request.player.activity === 'playing' &&
    request.map.mode === 'scrimcomp2v2'
  ) {
    if (request.map.phase === 'warmup') {
      setActivity({
        details: `Wingman`,
        state: 'Warmup',
        largeImageKey: request.map.name.split('/').reverse()[0],
        largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
        smallImageKey: request.player.team.toLowerCase(),
        smallImageText: `Playing on Team: ${request.player.team}`,
        timestamp: getTimestamp(),
      });
    }
    if (request.map.phase === 'live') {
      if (request.player.team === 'CT') {
        if (request.round.bomb) {
          setActivity({
            details: `Wingman ${request.map.team_ct.score}:${
              request.map.team_t.score
            }`,
            state: `Bomb ${capitalFirst(request.round.bomb)}`,
            largeImageKey: request.map.name.split('/').reverse()[0],
            largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
            smallImageKey: request.player.team.toLowerCase(),
            smallImageText: `Playing on Team: ${request.player.team}`,
            timestamp: getTimestamp(),
          });
          return;
        }
        setActivity({
          details: `Wingman ${request.map.team_ct.score}:${
            request.map.team_t.score
          }`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        if (request.round.bomb) {
          setActivity({
            details: `Wingman ${request.map.team_t.score}:${
              request.map.team_ct.score
            }`,
            state: `Bomb ${capitalFirst(request.round.bomb)}`,
            largeImageKey: request.map.name.split('/').reverse()[0],
            largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
            smallImageKey: request.player.team.toLowerCase(),
            smallImageText: `Playing on Team: ${request.player.team}`,
            timestamp: getTimestamp(),
          });
          return;
        }
        setActivity({
          details: `Wingman ${request.map.team_t.score}:${
            request.map.team_ct.score
          }`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
    if (request.map.phase === 'intermission') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `Wingman ${request.map.team_ct.score}:${
            request.map.team_t.score
          }`,
          state: 'Halftime',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `Wingman ${request.map.team_t.score}:${
            request.map.team_ct.score
          }`,
          state: 'Halftime',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
    if (request.map.phase === 'gameover') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `Wingman ${request.map.team_ct.score}:${
            request.map.team_t.score
          }`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `Wingman ${request.map.team_t.score}:${
            request.map.team_ct.score
          }`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
  }
  if (
    request.player.activity === 'playing' &&
    request.map.mode !== 'scrimcomp2v2'
  ) {
    if (request.map.phase === 'warmup') {
      setActivity({
        details: `${capitalFirst(request.map.mode)}`,
        state: 'Warmup',
        largeImageKey: request.map.name.split('/').reverse()[0],
        largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
        smallImageKey: request.player.team.toLowerCase(),
        smallImageText: `Playing on Team: ${request.player.team}`,
        timestamp: getTimestamp(),
      });
    }
    if (request.map.phase === 'live') {
      if (request.player.team === 'CT') {
        if (request.round.bomb) {
          setActivity({
            details: `${capitalFirst(request.map.mode)} ${
              request.map.team_ct.score
            }:${request.map.team_t.score}`,
            state: `Bomb ${capitalFirst(request.round.bomb)}`,
            largeImageKey: request.map.name.split('/').reverse()[0],
            largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
            smallImageKey: request.player.team.toLowerCase(),
            smallImageText: `Playing on Team: ${request.player.team}`,
            timestamp: getTimestamp(),
          });
          return;
        }
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_ct.score
          }:${request.map.team_t.score}`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        if (request.round.bomb) {
          setActivity({
            details: `${capitalFirst(request.map.mode)} ${
              request.map.team_t.score
            }:${request.map.team_ct.score}`,
            state: `Bomb ${capitalFirst(request.round.bomb)}`,
            largeImageKey: request.map.name.split('/').reverse()[0],
            largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
            smallImageKey: request.player.team.toLowerCase(),
            smallImageText: `Playing on Team: ${request.player.team}`,
            timestamp: getTimestamp(),
          });
          return;
        }
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_t.score
          }:${request.map.team_ct.score}`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
    if (request.map.phase === 'intermission') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_ct.score
          }:${request.map.team_t.score}`,
          state: 'Halftime',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_t.score
          }:${request.map.team_ct.score}`,
          state: 'Halftime',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
    if (request.map.phase === 'gameover') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_ct.score
          }:${request.map.team_t.score}`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `${capitalFirst(request.map.mode)} ${
            request.map.team_t.score
          }:${request.map.team_ct.score}`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
  }
  if (
    request.player.activity === 'playing' &&
    request.map.mode !== 'scrimcomp2v2'
  ) {
    if (request.map.phase === 'warmup') {
      setActivity({
        details: `${capitalFirst(request.map.mode)}`,
        state: 'Warmup',
        largeImageKey: request.map.name.split('/').reverse()[0],
        largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
        smallImageKey: request.player.team.toLowerCase(),
        smallImageText: `Playing on Team: ${request.player.team}`,
        timestamp: getTimestamp(),
      });
    }
    if (request.map.phase === 'live') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `${capitalFirst(request.map.mode)}`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `${capitalFirst(request.map.mode)}`,
          state: 'Playing',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
    if (request.map.phase === 'gameover') {
      if (request.player.team === 'CT') {
        setActivity({
          details: `${capitalFirst(request.map.mode)}`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
      if (request.player.team === 'T') {
        setActivity({
          details: `${capitalFirst(request.map.mode)}`,
          state: 'Match Over',
          largeImageKey: request.map.name.split('/').reverse()[0],
          largeImageText: `Map: ${request.map.name.split('/').reverse()[0]}`,
          smallImageKey: request.player.team.toLowerCase(),
          smallImageText: `Playing on Team: ${request.player.team}`,
          timestamp: getTimestamp(),
        });
      }
    }
  }
}

app.use(bodyParser());

app.post('/', function(req, res) {
  res.send('Got a POST request');
  parseActivity(req.body);
});

const isRunning = (query, cb) => {
  const { platform } = process;
  let cmd = '';
  switch (platform) {
    case 'win32':
      cmd = `tasklist`;
      break;
    case 'darwin':
      cmd = `ps -ax | grep ${query}`;
      break;
    case 'linux':
      cmd = `ps -A`;
      break;
    default:
      break;
  }
  exec(cmd, (err, stdout, stderr) => {
    cb(stdout.toLowerCase().indexOf(query.toLowerCase()) > -1);
  });
};

setInterval(() => {
  isRunning('csgo.exe', status => {
    if (status) return;
    if (!status) rpc.clearActivity();
  });
}, 5e3);

app.listen(port);
rpc.login({ clientId }).catch(console.error);
